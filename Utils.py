#!/usr/bin/env python
# -*- coding: utf-8 -*-

import collections
import mojimoji
import io
import re

import numpy
from numpy import random

from Const import UNK_ID, BOS_ID, EOS_ID

digit_pattern = re.compile(r'\d+')


def open_file(path):
    return io.open(path, encoding='utf-8', errors='ignore')


def load_file(path):
    with open_file(path) as f:
        for line in f:
            line = digit_pattern.sub('#', line)
            line = mojimoji.zen_to_han(line, kana=False)
            words = line.rstrip().split(' ')
            yield words


def get_vocab(path, vocabsize, minfreq):
    counter = collections.Counter()
    for words in load_file(path):
        for w in words:
            counter[w] += 1

    vocab = [w for w, f 
             in counter.most_common(vocabsize) if f >= minfreq]
    return vocab


def save_vocab(path, vocab):
    with open(path, 'w') as f:
        for w in vocab:
            f.write(w)
            f.write('\n')


def word2id(path, vocab):
    w2id= {w:i+3 for i, w in enumerate(vocab)}
    w2id['BOS'] = BOS_ID
    w2id['EOS'] = EOS_ID
    data = []
    for words in load_file(path):
        seq = ([w2id.get(w, UNK_ID) for w in words])
        data.append(numpy.array(seq, dtype=numpy.int32))
    return data


def get_emb(path, vocab, units):
    w2id = load_emb(path)
    embsize = len(list(w2id.values())[0])
    assert embsize < units

    vocab.insert(UNK_ID, 'UNK')
    vocab.insert(BOS_ID, 'BOS')
    vocab.insert(EOS_ID, 'EOS')  

    pre_emb = numpy.array([w2id[w] if w in w2id.keys() 
                           else random.uniform(-1, 1, embsize) for w in vocab])

    if embsize < units:
        plus_emb = numpy.array([random.uniform(-1, 1, units-embsize)
                                for _ in range(len(vocab))])
        pre_emb = numpy.concatenate((pre_emb, plus_emb), axis=1)
    return pre_emb


def load_emb(path):
    w2id = {}
    f = open_file(path)
    f.readline() # skip first line
    for line in f:
        line = line.rstrip().split(' ')
        w2id[line[0]] = numpy.array(list(map(float, line[1:])), numpy.float32)
    return w2id 

